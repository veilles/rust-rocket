The purpose of this project is to learn the Rocket framework and improve my Rust.

The project serve static files.

Set the environments variable `PATH_FOLDER` to the folder that will have your static files.

When using docker this variable is a docker argument.

## Run the server in dev env

 `cargo run` 

 **or** 

 `export ENV && ENV=dev cargo run`

## Run the server in prod env
 `export ENV && ENV=prod cargo run`

## Build the project

`cargo build`

## Build the project for a release

`cargo build --release`


## Using Docker

### Build the docker image

Will build a release, deploy it under a reverse proxy.

You can access the app at `http://localhost`.

`docker-compose up -d --build`
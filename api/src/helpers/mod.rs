use crate::apis::SearchResponse;
use std::env;
use std::fs;
use std::fs::ReadDir;
use std::path::PathBuf;

pub fn search_files(text: &str) -> Vec<SearchResponse> {
    let folder_path: String = env::var("FOLDER_PATH").expect("Env var FOLDER_PATH not set");

    let dir: ReadDir = fs::read_dir(folder_path.trim()).unwrap();
    let files: Vec<SearchResponse> = crawl_on_folder(dir, text);
    return files;
}

fn crawl_on_folder(dir: ReadDir, text: &str) -> Vec<SearchResponse> {
    let mut response: Vec<SearchResponse> = vec![];

    for path in dir {
        let tmp_path: PathBuf = path.unwrap().path();
        let path_string: &str = &tmp_path.to_str().unwrap().trim();
        let is_dir: bool = tmp_path.is_dir();

        if is_dir == true {
            let dir: ReadDir = fs::read_dir(path_string).unwrap();
            let mut files: Vec<SearchResponse> = crawl_on_folder(dir, text);
            response.append(&mut files);
        }

        let file_path: Option<SearchResponse> = check_file(text, tmp_path);

        match file_path {
            Some(x) => response.push(x),
            None => (),
        }
    }
    response
}

fn check_file(text: &str, path: PathBuf) -> Option<SearchResponse> {
    let is_file: bool = path.is_file();
    let extension: &str;

    match path.extension() {
        Some(x) => extension = x.to_str().unwrap(),
        None => extension = "",
    }

    if is_file == true {
        if extension == "html" {
            let file_content: String =
                fs::read_to_string(&path).expect("Should have been able to read the file {}");

            if file_content.contains(text) {
                let path_string: String = path.to_str().unwrap().trim().to_string();
                let search_response: SearchResponse = SearchResponse {
                    path: path_string,
                    title: String::new(),
                };
                return Some(search_response);
            }
        }
    }
    None
}

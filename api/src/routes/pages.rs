use rocket::fs::NamedFile;
use std::env;
use std::path::{Path, PathBuf};

#[get("/<path..>")]
pub async fn base_routes(path: PathBuf) -> Option<NamedFile> {
    let folder_path: String = env::var("FOLDER_PATH").expect("Env var FOLDER_PATH not set");
    NamedFile::open(Path::new(folder_path.trim()).join(path))
        .await
        .ok()
}

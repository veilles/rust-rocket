use crate::helpers::search_files;
use rocket::serde::{json::Json, Deserialize, Serialize};

#[derive(Deserialize, Serialize)]
#[serde(crate = "rocket::serde")]
pub struct SearchParams<'r> {
    text: &'r str,
}

#[derive(Debug, Deserialize, Serialize)]
#[serde(crate = "rocket::serde")]
pub struct SearchResponse {
    pub path: String,
    pub title: String,
}

#[post("/search", format = "application/json", data = "<body>")]
pub fn search(body: Json<SearchParams>) -> Json<Vec<SearchResponse>> {
    let data: Vec<SearchResponse> = search_files(body.text);
    Json(data)
}

#[macro_use]
extern crate rocket;

use rocket::{Build, Rocket};

mod apis;
mod helpers;
mod routes;

use rocket::fairing::{Fairing, Info, Kind};
use rocket::http::{ContentType, Header, Method};
use rocket::{Request, Response};

pub struct CorsFairing;

#[rocket::async_trait]
impl Fairing for CorsFairing {
    fn info(&self) -> Info {
        Info {
            name: "Add CORS headers to requests",
            kind: Kind::Response,
        }
    }

    async fn on_response<'r>(&self, request: &'r Request<'_>, response: &mut Response<'r>) {
        // Add CORS headers to allow all origins to all outgoing requests
        response.set_header(Header::new("Access-Control-Allow-Origin", "*"));
        response.set_header(Header::new(
            "Access-Control-Allow-Methods",
            "POST, GET, OPTIONS",
        ));
        response.set_header(Header::new("Access-Control-Allow-Headers", "Content-Type"));
        response.set_header(Header::new("Access-Control-Allow-Credentials", "true"));

        // Respond to all `OPTIONS` requests with a `204` (no content) status
        if request.method() == Method::Options {
            response.set_header(ContentType::Plain);
        }
    }
}

#[launch]
fn rocket() -> _ {
    let rocket: Rocket<Build> = rocket::build();
    rocket
        .mount(
            "/",
            routes![routes::pages::base_routes, routes::all_options],
        )
        .mount("/api", routes![apis::search])
        .attach(CorsFairing)
        .register("/", catchers![routes::errors::not_found])
}

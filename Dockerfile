ARG FOLDER_PATH=./doc
FROM rust:1.63.0 as builder
ARG FOLDER_PATH

COPY ./api ./
RUN echo ${FOLDER_PATH}
COPY $FOLDER_PATH $FOLDER_PATH
RUN cargo install --path .

FROM debian:buster-slim as runner
ARG FOLDER_PATH
COPY --from=builder /usr/local/cargo/bin/rust-rocket /usr/local/bin/rust-rocket
COPY --from=builder ${FOLDER_PATH} ${FOLDER_PATH}

ENV ROCKET_ADDRESS=0.0.0.0
ENV ROCKET_PORT=8000
ENV FOLDER_PATH=${FOLDER_PATH}
EXPOSE 8000

CMD ["rust-rocket"]
